import com.typesafe.sbt.packager.docker._
import sbt.Keys.libraryDependencies

name := "spark-log-analysis"
scalaVersion := "2.12.7"
version := "0.1"

/*
  ~~~ NOTE manual swapping of dependency lines is needed to run `sbt assembly`  ~~~
Can't quite figure out how to get docker packaging and fat jar with dependency exclusions working nicely together... pull requests welcome :)
For the time being, you need to comment and uncomment the spark dependency lines as appropriate for what you are building.
For `sbt docker:publishLocal`, make sure the spark dependency is not marked as "provided".
For `sbt assembly`, make sure the spark dependency is marked as "provided".
*/
// libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.4.4" % "provided" // this line should be uncommented when running `sbt assembly`
libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.4.4" // this line should be uncommented when running `sbt docker:*`
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.0" % "test"

libraryDependencies += "org.mockito" %% "mockito-scala" % "1.14.8"
libraryDependencies += "com.typesafe" % "config" % "1.4.0"
libraryDependencies += "com.github.scopt" %% "scopt" % "3.7.1"

scalastyleFailOnError := true

// docker
packageName in Docker := "spark-log-analysis"
version in Docker := version.value
daemonUserUid in Docker := None
daemonUser in Docker := "daemon"
dockerBaseImage := "openjdk:8"
dockerRepository := Some("registry.gitlab.com/tswinson")
dockerCommands ++= Seq(Cmd("USER", "daemon"))

enablePlugins(DockerPlugin, JavaAppPackaging)

// packaging a fat jar
mainClass in assembly := Some("com.tswinson.data.WebLogAnalysisApp")
assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)
test in assembly := {}
run in Compile := Defaults.runTask(fullClasspath in Compile, mainClass in(Compile, run), runner in(Compile, run)).evaluated
runMain in Compile := Defaults.runMainTask(fullClasspath in Compile, runner in(Compile, run)).evaluated

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", _@_*) => MergeStrategy.discard
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}
