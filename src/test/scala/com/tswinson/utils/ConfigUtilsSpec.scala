package com.tswinson.utils

import com.typesafe.config.{ConfigFactory, ConfigValueFactory}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class ConfigUtilsSpec extends AnyFlatSpec with Matchers {

  import ConfigUtils._

  private val config = ConfigFactory.empty
    .withValue("key1", ConfigValueFactory.fromAnyRef("value1"))
    .withValue("key2", ConfigValueFactory.fromAnyRef(2))
    .withValue("key3", ConfigValueFactory.fromAnyRef(3.0))

  private val nonEmptyString = (s: String) => s.nonEmpty
  private val nonZeroInt = (i: Int) => i != 0
  private val nonZeroDouble = (i: Double) => i != 0

  "getAs" should "return the config value with specified type for the given key" in {
    config.getAs[String]("key1", nonEmptyString) shouldBe "value1"
    config.getAs[Int]("key2", nonZeroInt) shouldBe 2
    config.getAs[Double]("key3", nonZeroDouble) shouldBe 3.0
  }

  it should "throw exception if key does not exist" in {
    the[Exception] thrownBy {
      config.getAs[String]("bad-key", nonEmptyString)
    } should have message "could not find config value for [bad-key] in config"
  }

  it should "throw exception if the value is not valid" in {
    the[Exception] thrownBy {
      config.getAs[String]("key1", _.isEmpty)
    } should have message "config value for [key1] is invalid"
  }

  it should "throw exception if value has wrong type" in {
    the[Exception] thrownBy {
      config.getAs[Int]("key1", nonZeroInt)
    } should have message "config value for [key1] does not have specified type"
  }
}
