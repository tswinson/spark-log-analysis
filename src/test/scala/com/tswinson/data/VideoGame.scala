package com.tswinson.data

case class VideoGame(title: String, platform: String, year: Int)
