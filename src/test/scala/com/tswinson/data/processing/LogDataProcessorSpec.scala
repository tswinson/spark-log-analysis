package com.tswinson.data.processing

import java.nio.file.Files

import com.tswinson.data.VideoGame
import com.tswinson.data.output.OutputWriter
import com.tswinson.data.parsers.LogParser
import org.apache.spark.sql.{Dataset, Encoder, Encoders, SparkSession}
import org.scalatest.flatspec.AsyncFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.Future
import scala.util.Try

class LogDataProcessorSpec extends AsyncFlatSpec with Matchers {
  private implicit val spark: SparkSession = SparkSession.builder.appName("Tests").master("local[1]").getOrCreate()

  import spark.implicits._

  "process" should "process log file according to processing definitions including multiple output writers per processing function" in {
    val logFile = Files.createTempFile("LogDataProcessorSpec", ".log")
    sys.addShutdownHook {
      Files.delete(logFile)
    }

    val videoGames = Seq(
      "Ico PS2 2001",
      "Uncharted PS3 2007",
      "Halo Xbox 2001"
    )
    Files.write(logFile, videoGames.mkString("\n").getBytes)

    val logParser = new LogParser[VideoGame] {
      override protected implicit def encoderT: Encoder[VideoGame] = Encoders.product[VideoGame]

      override def parseLine(line: String): Try[VideoGame] = Try {
        line.split(" ") match {
          case Array(title, platform, year) => VideoGame(title, platform, year.toInt)
        }
      }
    }

    // finds the first/last game title by lexicographical order
    val firstTitle = (ds: Dataset[VideoGame]) => ds.map(_.title).collect().min
    val lastTitle = (ds: Dataset[VideoGame]) => ds.map(_.title).collect().max

    val inMemoryOutputWriter = new OutputWriter[String] {
      private val sb = new StringBuilder

      def getValue: String = {
        sb.toString
      }

      override def writeOutput(data: String): Future[Unit] = {
        Future {
          sb.append(data + "\n")
        }
      }
    }

    // contrived example to demonstrate 2nd output writer
    val lengthOutputWriter = new OutputWriter[String] {
      private val sb = new StringBuilder

      def getValue: Int = {
        sb.toString.length
      }

      override def writeOutput(data: String): Future[Unit] = {
        Future {
          sb.append(data)
        }
      }
    }

    val processingDefinitions = Seq(
      DataProcessingDefinition(firstTitle, Seq(inMemoryOutputWriter)),
      DataProcessingDefinition(lastTitle, Seq(inMemoryOutputWriter, lengthOutputWriter))
    )

    val result = LogDataProcessor(logFile.toFile.getAbsolutePath, logParser, processingDefinitions).process()
    result.map { _ =>
      // check that output writers collected the data
      inMemoryOutputWriter.getValue shouldBe "Halo\nUncharted\n" // 1 title added by each processing function
      lengthOutputWriter.getValue shouldBe 9 // length of "Uncharted", added only by last processing function
    }

  }

}
