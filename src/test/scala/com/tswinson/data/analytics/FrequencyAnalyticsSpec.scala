package com.tswinson.data.analytics

import com.tswinson.data.VideoGame
import com.tswinson.data.analytics.FrequencyAnalytics._
import org.apache.spark.sql.SparkSession
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class FrequencyAnalyticsSpec extends AnyFlatSpec with Matchers {
  private implicit val spark: SparkSession = SparkSession.builder.appName("Tests").master("local[1]").getOrCreate()

  import spark.implicits._

  "calcMostFrequentBy" should "return [up to] the specified number of most frequent items as selected by itemSelector (desc)," +
    "for each element selected by the outerGroupingSelector (asc)" in {

    val ds = Seq(
      VideoGame("Ico", "PS2", 2001),
      VideoGame("God of War", "PS2", 2005),
      VideoGame("God of War II", "PS2", 2007),
      VideoGame("God of War III", "PS3", 2010),
      VideoGame("LittleBigPlanet", "PS3", 2008),
      VideoGame("Uncharted 2", "PS3", 2009),
      VideoGame("The Last of Us", "PS3", 2013),
      VideoGame("The Last of Us 2", "PS4", 2020),
      VideoGame("Uncharted 4", "PS4", 2016),
      VideoGame("Halo", "Xbox", 2001),
      VideoGame("Super Mario Brothers", "NES", 1985),
      VideoGame("Super Mario Brothers 2", "NES", 1988),
      VideoGame("Super Mario Brothers 3", "NES", 1988),
      VideoGame("Duck Hunt", "NES", 1984),
      VideoGame("Super Mario World", "SNES", 1990)
    ).toDS()

    val decadeSelector = (vg: VideoGame) => (vg.year / 10 * 10) + "s"
    val platformSelector = (vg: VideoGame) => vg.platform

    calcMostFrequentBy(numTop = 1, decadeSelector, platformSelector)(ds) should contain theSameElementsInOrderAs
      Seq(
        ("1980s", Seq(Occurrences("NES", 4))),
        ("1990s", Seq(Occurrences("SNES", 1))),
        ("2000s", Seq(Occurrences("PS2", 3))),
        ("2010s", Seq(Occurrences("PS3", 2))),
        ("2020s", Seq(Occurrences("PS4", 1)))
      )

    calcMostFrequentBy(numTop = 2, decadeSelector, platformSelector)(ds) should contain theSameElementsInOrderAs
      Seq(
        ("1980s", Seq(Occurrences("NES", 4))),
        ("1990s", Seq(Occurrences("SNES", 1))),
        ("2000s", Seq(Occurrences("PS2", 3), Occurrences("PS3", 2))),
        ("2010s", Seq(Occurrences("PS3", 2), Occurrences("PS4", 1))),
        ("2020s", Seq(Occurrences("PS4", 1)))
      )

    calcMostFrequentBy(numTop = 100, decadeSelector, platformSelector)(ds) should contain theSameElementsInOrderAs
      Seq(
        ("1980s", Seq(Occurrences("NES", 4))),
        ("1990s", Seq(Occurrences("SNES", 1))),
        ("2000s", Seq(Occurrences("PS2", 3), Occurrences("PS3", 2), Occurrences("Xbox", 1))),
        ("2010s", Seq(Occurrences("PS3", 2), Occurrences("PS4", 1))),
        ("2020s", Seq(Occurrences("PS4", 1)))
      )
  }

}
