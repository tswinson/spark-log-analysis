package com.tswinson.data

import java.nio.file.Files

import org.mockito.MockitoSugar
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.slf4j.Logger

class WebLogAnalysisAppSpec extends AnyFlatSpec with Matchers with MockitoSugar {

  "run" should "compute the top-n items from the input log" in {
    val logFile = Files.createTempFile("WebLogAnalysisAppSpec", ".log")
    sys.addShutdownHook {
      Files.delete(logFile)
    }

    val lines = Seq(
      """10.0.0.6 - - [03/Jul/2020:00:00:01 -0400] "GET /history/apollo/ HTTP/1.0" 500 -""",
      """10.0.0.3 - - [03/Jul/2020:00:00:01 -0400] "GET /history/apollo/ HTTP/1.0" 200 6245""",
      """10.0.0.3 - - [03/Jul/2020:00:00:06 -0400] "GET /shuttle/countdown/ HTTP/1.0" 201 3985""",
      """example.com - - [03/Jul/2020:00:03:21 -0400] "GET /shuttle/countdown/ HTTP/1.0" 200 3985""",
      """10.0.0.2 - - [03/Jul/2020:00:00:09 -0400] "GET /shuttle/missions/sts-73/ HTTP/1.0" 200 4085""",
      """10.0.0.3 - - [03/Jul/2020:00:00:11 -0400] "GET /shuttle/countdown/ HTTP/1.0" 304 0""",
      """10.0.0.2 - - [03/Jul/2020:00:00:11 -0400] "GET /shuttle/countdown/ HTTP/1.0" 304 0""",
      """10.0.0.4 - - [02/Jul/2020:00:00:12 -0400] "GET /images/NASA-logosmall.gif HTTP/1.0" 304 0""",
      """10.0.0.4 - - [02/Jul/2020:00:00:13 -0400] "GET /images/NASA-logosmall HTTP/1.0" 404 1""",
      """10.0.0.4 - - [02/Jul/2020:00:00:12 -0400] "GET /shuttle/countdown/video/livevideo.gif HTTP/1.0" 200 0""",
      """10.0.0.4 - - [02/Jul/2020:00:00:12 -0400] "GET /shuttle/countdown/ HTTP/1.0" 200 3985""",
      """10.0.0.5 - - [02/Jul/2020:00:00:12 -0400] "GET /shuttle/countdown/ HTTP/1.0" 200 3985""",
      """example.com - - [02/Jul/2020:00:00:12 -0400] "GET /shuttle/countdown/ HTTP/1.0" 200 3985""",
      """example.com - - [02/Jul/2020:00:00:13 -0400] "GET / HTTP/1.0" 404 -""",
      """example.com - - [02/Jul/2020:00:00:14 -0400] "GET / HTTP/1.0" 404 -"""
    )

    Files.write(logFile, lines.mkString("\n").getBytes)

    val logger = mock[Logger]

    val params = WebLogAnalysisAppParams(
      inputPath = logFile.toFile.getAbsolutePath,
      numTopUrls = 1,
      numTopVisitors = 3,
      numTopResponseCodes = 2,
      localMode = true
    )

    val app = new WebLogAnalysisApp(params, logger)
    app.run()

    verify(logger, times(1)).info("most frequent url(s) for date: 2020-07-02")
    verify(logger, times(1)).info("most frequent visitor(s) for date: 2020-07-02")
    verify(logger, times(1)).info("most frequent response code(s) for date: 2020-07-02")

    verify(logger, times(1)).info("most frequent url(s) for date: 2020-07-03")
    verify(logger, times(1)).info("most frequent visitor(s) for date: 2020-07-03")
    verify(logger, times(1)).info("most frequent response code(s) for date: 2020-07-03")

    verify(logger, times(1)).info("most frequent url(s) for response code: 200")
    verify(logger, times(1)).info("most frequent url(s) for response code: 201")
    verify(logger, times(1)).info("most frequent url(s) for response code: 304")
    verify(logger, times(1)).info("most frequent url(s) for response code: 404")
    verify(logger, times(1)).info("most frequent url(s) for response code: 500")
    verify(logger, times(1)).info("\tvisitor: 10.0.0.4; count: 4")
    verify(logger, times(1)).info("\tvisitor: example.com; count: 3")
    verify(logger, times(1)).info("\tvisitor: 10.0.0.5; count: 1")
    verify(logger, times(1)).info("\tvisitor: 10.0.0.3; count: 3")
    verify(logger, times(1)).info("\tvisitor: 10.0.0.2; count: 2")
    verify(logger, times(1)).info("\tvisitor: 10.0.0.6; count: 1")
    verify(logger, times(1)).info("\turl: /shuttle/countdown/; count: 3")
    verify(logger, times(2)).info("\turl: /shuttle/countdown/; count: 4")
    verify(logger, times(1)).info("\turl: /shuttle/countdown/; count: 1")
    verify(logger, times(1)).info("\turl: /shuttle/countdown/; count: 2")
    verify(logger, times(1)).info("\turl: /history/apollo/; count: 1")
    verify(logger, times(1)).info("\turl: /; count: 2")
    verify(logger, times(1)).info("\tresponse code: 200; count: 4")
    verify(logger, times(1)).info("\tresponse code: 404; count: 3")
    verify(logger, times(1)).info("\tresponse code: 200; count: 3")
    verify(logger, times(1)).info("\tresponse code: 304; count: 2")
  }

  it should "log the parameters being used" in {
    val logFile = Files.createTempFile("WebLogAnalysisAppSpec", ".log")
    sys.addShutdownHook {
      Files.delete(logFile)
    }

    Files.write(logFile, Seq.empty[String].mkString("\n").getBytes)

    val logger = mock[Logger]

    val params = WebLogAnalysisAppParams(
      inputPath = logFile.toFile.getAbsolutePath,
      numTopUrls = 42,
      numTopVisitors = 500,
      numTopResponseCodes = 3,
      localMode = true
    )

    val app = new WebLogAnalysisApp(params, logger)
    app.run()

    verify(logger, times(1)).info("Using params:")
    verify(logger, times(1)).info(s"\tinput-path = ${params.inputPath}")
    verify(logger, times(1)).info("\tnum-top-urls = 42")
    verify(logger, times(1)).info("\tnum-top-visitors = 500")
    verify(logger, times(1)).info("\tnum-top-response-codes = 3")
    verify(logger, times(1)).info("\tlocal-mode = true")
  }

}
