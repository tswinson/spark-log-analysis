package com.tswinson.data.output

import com.tswinson.data.analytics.Occurrences
import org.mockito.{ArgumentMatchersSugar, MockitoSugar}
import org.scalatest.flatspec.AsyncFlatSpec
import org.scalatest.matchers.should.Matchers
import org.slf4j.Logger

class GroupedOccurrencesLogOutputWriterSpec extends AsyncFlatSpec with Matchers with MockitoSugar with ArgumentMatchersSugar {

  "writeOutput" should "send output to logs in expected format" in {
    val logger = mock[Logger]

    val outputWriter = GroupedOccurrencesLogOutputWriter(logger)("decade", "platform")

    val data = Seq(
      ("2000s", Seq(Occurrences("PSP", 45), Occurrences("Dreamcast", 30))),
      ("2010s", Seq(Occurrences("PS Vita", 15)))
    )

    outputWriter.writeOutput(data).map { x =>
      verify(logger, times(1)).info("most frequent platform(s) for decade: 2000s")
      verify(logger, times(1)).info("\tplatform: PSP; count: 45")
      verify(logger, times(1)).info("\tplatform: Dreamcast; count: 30")
      verify(logger, times(1)).info("most frequent platform(s) for decade: 2010s")
      verify(logger, times(1)).info("\tplatform: PS Vita; count: 15")
      x shouldBe()
    }

  }
}
