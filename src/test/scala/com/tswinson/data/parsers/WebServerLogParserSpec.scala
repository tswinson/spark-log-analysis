package com.tswinson.data.parsers

import java.sql.Timestamp

import com.tswinson.data.parsers.WebServerLogParser._
import org.apache.spark.sql.SparkSession
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.util.Success

class WebServerLogParserSpec extends AnyFlatSpec with Matchers {
  private implicit val spark: SparkSession = SparkSession.builder.appName("Tests").master("local[1]").getOrCreate()

  import spark.implicits._

  "parseLine" should "transform input into WebServerLogMessage" in {
    parseLine("""localhost - - [29/Jul/2020:08:30:00 -0400] "GET /index.html HTTP/1.0" 200 512""") shouldBe
      Success(
        WebServerLogMessage(
          "localhost",
          Timestamp.valueOf("2020-07-29 08:30:00"),
          Request("GET", "/index.html"),
          200,
          Some(512L)
        )
      )

    parseLine("""127.0.0.1 - - [28/Jun/2019:23:30:00 -0400] "GET /home HTTP/1.0" 100 0""") shouldBe
      Success(
        WebServerLogMessage(
          "127.0.0.1",
          Timestamp.valueOf("2019-06-28 23:30:00"),
          Request("GET", "/home"),
          100,
          None
        )
      )

    parseLine("""127.0.0.1 - - [28/Jun/2019:23:30:00 -0400] "GET /home HTTP/1.0" 599 -""") shouldBe
      Success(
        WebServerLogMessage(
          "127.0.0.1",
          Timestamp.valueOf("2019-06-28 23:30:00"),
          Request("GET", "/home"),
          599,
          None
        )
      )
  }

  it should "fail if timestamp not in expected format" in {
    parseLine("""localhost - - [2020-01-01 08:30:00] "GET /index.html HTTP/1.0" 200 512""").isFailure shouldBe true
  }

  it should "fail if http request not in expected format" in {
    parseLine("""127.0.0.1 - - [28/Jun/2019:23:30:00 -0400] "malformed" 404 -""").isFailure shouldBe true
  }

  it should "fail if http request is not a real HTTP verb" in {
    parseLine("""localhost - - [29/Jul/2020:08:30:00 -0400] "ORDER /index.html HTTP/1.0" 200 512""").isFailure shouldBe true
  }

  it should "fail if response code not in range of value http codes" in {
    parseLine("""localhost - - [29/Jul/2020:08:30:00 -0400] "GET /index.html HTTP/1.0" 99 512""").isFailure shouldBe true
    parseLine("""localhost - - [29/Jul/2020:08:30:00 -0400] "GET /index.html HTTP/1.0" 600 512""").isFailure shouldBe true
  }

  it should "fail if response code not an int" in {
    parseLine("""localhost - - [29/Jul/2020:08:30:00 -0400] "GET /index.html HTTP/1.0" ABC 512""").isFailure shouldBe true
  }

  "parseLog" should "parse all lines in dataset into specified type" in {
    val ds = Seq(
      """localhost - - [29/Jul/2020:08:30:00 -0400] "GET /index.html HTTP/1.0" 200 512""",
      """127.0.0.1 - - [28/Jun/2019:23:30:00 -0400] "GET /home HTTP/1.0" 100 0""",
      """127.0.0.1 - - [28/Jun/2019:23:30:00 -0400] "GET /home HTTP/1.0" 599 -"""
    ).toDS()

    val expected = Seq(
      WebServerLogMessage("localhost", Timestamp.valueOf("2020-07-29 08:30:00"), Request("GET", "/index.html"), 200, Some(512L)),
      WebServerLogMessage("127.0.0.1", Timestamp.valueOf("2019-06-28 23:30:00"), Request("GET", "/home"), 100, None),
      WebServerLogMessage("127.0.0.1", Timestamp.valueOf("2019-06-28 23:30:00"), Request("GET", "/home"), 599, None)
    ).toDS()

    parseLog(ds).collect() shouldBe expected.collect()
  }

  it should "exclude lines that cannot be parsed" in {
    val ds = Seq(
      """localhost - - [29/Jul/2020:08:30:00 -0400] "GET /index.html HTTP/1.0" 200 512""",
      """127.0.0.1 - - [28/Jun/2019:23:30:00 -0400] "GET /home HTTP/1.0" 100 0""",
      """127.0.0.1 - - [28/Jun/2019:23:30:00 -0400] "GET /home HTTP/1.0" 599 -""",
      """127.0.0.1 - - [28/Jun/2019:23:30:00 -0400] "malformed" 404 -""",
      """badly malformed"""
    ).toDS()

    val expected = Seq(
      WebServerLogMessage("localhost", Timestamp.valueOf("2020-07-29 08:30:00"), Request("GET", "/index.html"), 200, Some(512L)),
      WebServerLogMessage("127.0.0.1", Timestamp.valueOf("2019-06-28 23:30:00"), Request("GET", "/home"), 100, None),
      WebServerLogMessage("127.0.0.1", Timestamp.valueOf("2019-06-28 23:30:00"), Request("GET", "/home"), 599, None)
    ).toDS()

    parseLog(ds).collect() shouldBe expected.collect()
  }

}
