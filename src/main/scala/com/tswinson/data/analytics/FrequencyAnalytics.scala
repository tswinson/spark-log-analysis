package com.tswinson.data.analytics

import org.apache.spark.sql.{Dataset, Encoder, SparkSession}

object FrequencyAnalytics {
  /**
   * Type representing groups of item frequencies.
   */
  type GroupedOccurrences = Seq[(String, Seq[Occurrences])]

  /**
   * Computes the most frequent items per group from a dataset of parsed log lines.
   *
   * @param numTop                the number of items to include in each group
   * @param outerGroupingSelector function to select the name of the item for the outer grouping from the log data type
   * @param itemSelector          function to select the name representing the relevant item for frequency counts
   * @param logs                  the dataset of parsed logs
   * @param spark                 implicit spark session needs to be in scope
   * @tparam D the type of the parsed log lines
   * @return occurrences of the numTop items in each selected group, ordered descending by count
   *         groups are ordered lexicographically, ascending
   */
  def calcMostFrequentBy[D: Encoder](numTop: Int, outerGroupingSelector: D => String, itemSelector: D => String)(logs: Dataset[D])
                                    (implicit spark: SparkSession): GroupedOccurrences = {
    import spark.implicits._

    logs
      .groupByKey(outerGroupingSelector)
      .mapGroups { case (outerGroupItem, rows) =>
        val topCounts = rows
          .toSeq
          .groupBy(itemSelector)
          .mapValues(_.size)
          .toSeq
          .sortBy(-_._2)
          .take(numTop)
          .map { case (item, count) => Occurrences(item, count) }
        (outerGroupItem, topCounts)
      }
      .collect()
      .toSeq
      .sortBy(_._1)
  }
}
