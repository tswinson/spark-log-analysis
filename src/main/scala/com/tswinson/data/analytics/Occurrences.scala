package com.tswinson.data.analytics

// represents number of occurrences of a specific instance of an item
// for example, Occurrences("10.0.0.1", 123) means 123 occurrences of the specific instance "10.0.0.1" of the "IP address" item
case class Occurrences(name: String, count: Long)
