package com.tswinson.data.parsers

import org.apache.spark.sql.{Dataset, Encoder, SparkSession}
import org.slf4j.LoggerFactory

import scala.util.{Failure, Success, Try}

/**
 * This is a trait that can be used to create log parsers for specific log formats.
 *
 * @tparam T the output type of the parsed logs
 */
trait LogParser[T] extends Serializable {

  private val logger = LoggerFactory.getLogger(getClass.getName)

  /**
   * @return the Spark encoder for the parsed log output type
   */
  protected implicit def encoderT: Encoder[T]

  /**
   * Converts each row of the dataset into the parsed output type.
   *
   * @param ds    the input dataset of strings (log messages)
   * @param spark the spark session needs to be implicitly in scope
   * @return a dataset of parsed log messages, in the desired output type
   */
  def parseLog(ds: Dataset[String])(implicit spark: SparkSession): Dataset[T] = {
    ds.flatMap { line =>
      parseLine(line) match {
        case Success(t) => Some(t)
        case Failure(e) =>
          logger.warn(s"Parsing error for line: [$line]; ${e.getMessage}")
          None
      }
    }
  }

  /**
   * Parses a string into the desired output type.
   *
   * @param line the input string to parse (typically a line from a log)
   * @return a Try of the output type, indicating success or failure of the parsing operation
   */
  def parseLine(line: String): Try[T]

}
