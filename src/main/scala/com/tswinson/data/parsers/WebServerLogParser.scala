package com.tswinson.data.parsers

import java.sql.Timestamp
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import com.tswinson.data.parsers
import org.apache.spark.sql.{Encoder, Encoders}

import scala.util.{Failure, Try}

/**
 * Implementation of a LogParser that parses web server logs generated in a specific format from a web server in the mid 1990s.
 */
object WebServerLogParser extends LogParser[WebServerLogMessage] {
  private val MinHttpCode = 100
  private val MaxHttpCode = 599
  private val HttpMethods = Set("GET", "POST", "PUT", "DELETE", "HEAD", "PATCH")
  private val HttpRequestPattern = """^(\S+)\s+(\S+).*$""".r
  private val LogLinePattern = """^(\S+) - - \[(.+)\] \"(.+)\" (\d{3}) (-|\d+)$""".r

  protected override implicit def encoderT: Encoder[WebServerLogMessage] = Encoders.product[WebServerLogMessage]

  override def parseLine(line: String): Try[WebServerLogMessage] = {
    line match {
      case LogLinePattern(client, ts, req, respCode, bytes) =>
        val result = for {
          timestamp <- parseTimestamp(ts)
          request <- parseRequest(req)
          responseCode <- parseResponseCode(respCode)
          numBytes = parseBytes(bytes)
        } yield parsers.WebServerLogMessage(client, timestamp, request, responseCode, numBytes)
        result.recoverWith { case e: Throwable => Failure(new Exception(s"Data extraction error for input: $line; ${e.getMessage}")) }
      case _ =>
        Failure(new Exception(s"Error parsing log line [$line]: unexpected format"))

    }
  }

  private def parseTimestamp(ts: String): Try[Timestamp] = {
    Try {
      Timestamp.valueOf(LocalDateTime.parse(ts, DateTimeFormatter.ofPattern("dd/MMM/yyyy:HH:mm:ss Z")))
    }
  }

  private def parseRequest(req: String): Try[Request] = {
    Try {
      req match {
        case HttpRequestPattern(verb, url) if HttpMethods.contains(verb) => Request(verb, url)
      }
    }
  }

  private def parseResponseCode(code: String): Try[Int] = {
    Try(code.toInt).filter(c => c >= MinHttpCode && c <= MaxHttpCode)
  }

  private def parseBytes(bytes: String): Option[Long] = {
    if (bytes == "-" || bytes == "0") {
      None
    } else {
      Try(bytes.toLong).toOption
    }
  }

}
