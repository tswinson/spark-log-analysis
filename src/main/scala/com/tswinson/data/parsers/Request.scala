package com.tswinson.data.parsers

// represents part of an HTTP request: verb and URL
case class Request(verb: String, url: String)
