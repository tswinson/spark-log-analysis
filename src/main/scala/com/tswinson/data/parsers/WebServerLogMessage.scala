package com.tswinson.data.parsers

import java.sql.Timestamp

// represents constituent parts of a web server log message
case class WebServerLogMessage(client: String, timestamp: Timestamp, request: Request, responseCode: Int, bytesRetrieved: Option[Long])
