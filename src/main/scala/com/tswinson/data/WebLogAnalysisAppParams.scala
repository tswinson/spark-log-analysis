package com.tswinson.data

// parameters for the web log analysis application
case class WebLogAnalysisAppParams(inputPath: String, numTopUrls: Int, numTopVisitors: Int, numTopResponseCodes: Int, localMode: Boolean)
