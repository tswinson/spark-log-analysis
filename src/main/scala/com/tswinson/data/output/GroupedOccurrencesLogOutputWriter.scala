package com.tswinson.data.output

import com.tswinson.data.analytics.FrequencyAnalytics.GroupedOccurrences
import com.tswinson.data.analytics.Occurrences
import org.slf4j.Logger

/**
 * This is a log writer that appends occurrence info about the most frequently occurring items for each outer group.
 *
 * @param logger          the slf4j logger in which to append
 * @param outerGroupLabel the name of the outer grouping (e.g. "date" for top urls by date)
 * @param innerGroupLabel the name of the inner group item (e.g. "url" for top urls by date)
 */
case class GroupedOccurrencesLogOutputWriter(logger: Logger)(outerGroupLabel: String, innerGroupLabel: String) extends LogOutputWriter[GroupedOccurrences](logger) {
  override def toLogMessages(data: GroupedOccurrences): Stream[String] = {
    data.toStream.flatMap { case (outerGroupItem, innerItems) =>
      s"most frequent $innerGroupLabel(s) for $outerGroupLabel: $outerGroupItem" +: innerItems.toStream.map { case Occurrences(name, count) =>
        s"\t$innerGroupLabel: $name; count: $count"
      }
    }
  }
}
