package com.tswinson.data.output

import org.slf4j.Logger

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

abstract class LogOutputWriter[T](logger: Logger) extends OutputWriter[T] {

  /**
   * Converts data into a stream of strings, which will be written to the log.
   *
   * @param data arbitrary data that needs to be converted to strings
   * @return Stream of strings representing the input data
   */
  protected def toLogMessages(data: T): Stream[String]

  /**
   * Asynchronously converts the data to strings and then writes them to the logger.
   *
   * @param data arbitrary input data that should be logged
   * @return Future indicating success of failure
   */
  def writeOutput(data: T): Future[Unit] = Future {
    toLogMessages(data).foreach { msg =>
      logger.info(msg)
    }
  }
}
