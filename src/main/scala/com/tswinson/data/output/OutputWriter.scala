package com.tswinson.data.output

import scala.concurrent.Future

/**
 * This is the trait that can be used to create output writers for arbitrary data.
 * For example, data can be written to a log, a database, a web service, etc.
 *
 * @tparam T the type of the input data to be written
 */
trait OutputWriter[T] {

  /**
   * Asynchronously writes the input data to a destination.
   *
   * @param data arbitrary input data that should be written or sent elsewhere
   * @return Future indicating success of failure of the write operation
   */
  def writeOutput(data: T): Future[Unit]

}
