package com.tswinson.data

import java.sql.Date

import com.tswinson.data.analytics.FrequencyAnalytics._
import com.tswinson.data.output.GroupedOccurrencesLogOutputWriter
import com.tswinson.data.parsers.{WebServerLogMessage, WebServerLogParser}
import com.tswinson.data.processing.{DataProcessingDefinition, LogDataProcessor}
import com.tswinson.utils.ConfigUtils._
import com.typesafe.config.ConfigFactory
import org.apache.spark.sql.SparkSession
import org.slf4j.{Logger, LoggerFactory}

/**
 * Analyzes web server logs and computes the top-N most frequent urls, visitors and response codes per day.
 * Also computes the top-N most frequent url by response code.
 *
 * For the purpose of this project, results are written to the application's SLF4J logger through the LogOutputWriter.
 * Additional OutputWriters could be implemented as desired, e.g. to write to a database or send results to a web service.
 *
 * Program arguments can be used to specify the value of `N` for each item type (url, visitor, response code).
 * If arguments are not provided, default values from reference.conf are used.
 *
 * Specify --local-mode when running locally, and omit for running on a Spark cluster.
 *
 * If --input-path is not specified, then log location defaults to NASA web server logs from the Internet Traffic Archive (FTP location).
 */
object WebLogAnalysisApp {
  def main(args: Array[String]) {
    val config = ConfigFactory.load().getConfig("web-server-log-analysis")
    val logger = LoggerFactory.getLogger(getClass.getName)

    val defaultInputPath = config.getAs[String]("input-path", _.nonEmpty)
    val defaultNumTopUrls = config.getAs[Int]("num-top-urls", _ > 0)
    val defaultNumTopVisitors = config.getAs[Int]("num-top-visitors", _ > 0)
    val defaultNumTopResponseCodes = config.getAs[Int]("num-top-response-codes", _ > 0)

    val parser = new scopt.OptionParser[WebLogAnalysisAppParams]("Web Server Log Analysis") {
      head("WebServerLogAnalysisApp", "0.1")

      opt[String]('i', "input-path")
        .action((path, params) => params.copy(inputPath = path))
        .text("path for log input file")

      opt[Int]('u', "num-top-urls")
        .action((n, params) => params.copy(numTopUrls = n))
        .text("number of most frequent urls to find")

      opt[Int]('c', "num-top-visitors")
        .action((n, params) => params.copy(numTopVisitors = n))
        .text("number of most frequent visitors (clients) to find")

      opt[Int]('r', "num-top-response-codes")
        .action((n, params) => params.copy(numTopResponseCodes = n))
        .text("number of most frequent response codes to find")

      opt[Unit]('l', "local-mode")
        .action((_, params) => params.copy(localMode = true))
        .text("run spark in local mode")
    }

    val defaultParams = WebLogAnalysisAppParams(
      inputPath = defaultInputPath,
      numTopUrls = defaultNumTopUrls,
      numTopVisitors = defaultNumTopVisitors,
      numTopResponseCodes = defaultNumTopResponseCodes,
      localMode = false
    )

    parser.parse(args, defaultParams) match {
      case Some(params) => new WebLogAnalysisApp(params, logger).run()

      case None => // arguments are bad, error message will have been displayed
    }
  }
}

class WebLogAnalysisApp(params: WebLogAnalysisAppParams, logger: Logger) {

  // processing definitions can be added or modified here
  private def dataProcessingDefintions(implicit spark: SparkSession) = {
    import spark.implicits._

    val logWriter = GroupedOccurrencesLogOutputWriter(logger) _

    val mostFreqVisitorsByDateFn = calcMostFrequentBy(
      numTop = params.numTopVisitors,
      outerGroupingSelector = (m: WebServerLogMessage) => new Date(m.timestamp.getTime).toString,
      itemSelector = (m: WebServerLogMessage) => m.client
    ) _

    val mostFreqUrlsByDateFn = calcMostFrequentBy(
      numTop = params.numTopUrls,
      outerGroupingSelector = (m: WebServerLogMessage) => new Date(m.timestamp.getTime).toString,
      itemSelector = (m: WebServerLogMessage) => m.request.url
    ) _

    val mostFreqResponseCodeByDateFn = calcMostFrequentBy(
      numTop = params.numTopResponseCodes,
      outerGroupingSelector = (m: WebServerLogMessage) => new Date(m.timestamp.getTime).toString,
      itemSelector = (m: WebServerLogMessage) => m.responseCode.toString
    ) _

    val mostFreqUrlsByCodeFn = calcMostFrequentBy(
      numTop = params.numTopUrls,
      outerGroupingSelector = (m: WebServerLogMessage) => m.responseCode.toString,
      itemSelector = (m: WebServerLogMessage) => m.request.url
    ) _

    Seq(
      DataProcessingDefinition(mostFreqVisitorsByDateFn, Seq(logWriter("date", "visitor"))),
      DataProcessingDefinition(mostFreqUrlsByDateFn, Seq(logWriter("date", "url"))),
      DataProcessingDefinition(mostFreqResponseCodeByDateFn, Seq(logWriter("date", "response code"))),
      DataProcessingDefinition(mostFreqUrlsByCodeFn, Seq(logWriter("response code", "url")))
    )
  }

  // runs the spark job to process web log data
  def run(): Unit = {
    val name = "WebLogAnalysisApp"
    logger.info(s"Starting $name")
    sys.addShutdownHook(logger.info(s"Ending $name"))

    logParams()

    implicit val spark: SparkSession = if (params.localMode) {
      logger.warn("Starting spark in local mode with master=local[*]")
      SparkSession.builder.appName(name).master("local[*]").getOrCreate()
    } else {
      SparkSession.builder.appName(name).getOrCreate()
    }

    val logFile = params.inputPath
    val processor = LogDataProcessor(logFile, WebServerLogParser, dataProcessingDefintions)
    processor.process()
    spark.stop()
  }

  private def logParams(): Unit = {
    logger.info("Using params:")
    logger.info(s"\tinput-path = ${params.inputPath}")
    logger.info(s"\tnum-top-urls = ${params.numTopUrls}")
    logger.info(s"\tnum-top-visitors = ${params.numTopVisitors}")
    logger.info(s"\tnum-top-response-codes = ${params.numTopResponseCodes}")
    logger.info(s"\tlocal-mode = ${params.localMode}")
  }

}
