package com.tswinson.data.processing

import com.tswinson.data.output.OutputWriter
import org.apache.spark.sql.Dataset

/**
 * Defines how to process a dataset and specifies where to send the output.
 *
 * @param fn            a function that converts the input dataset into the desired output type O
 * @param outputWriters the writers in which to send the output data
 * @tparam D the type of the data contained in the Dataset
 * @tparam O the desired output type
 */
case class DataProcessingDefinition[D, O](fn: Dataset[D] => O, outputWriters: Seq[OutputWriter[O]])
