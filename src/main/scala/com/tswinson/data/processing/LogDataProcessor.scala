package com.tswinson.data.processing

import com.tswinson.data.parsers.LogParser
import org.apache.spark.sql.SparkSession

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
 * This processes log data according to the provided processing definitions. It parses the input logs and processes them according
 * to the provided processing definitions. Text files are the only supported log file input format at this time.
 *
 * @param inputPath             location of the (text-formatted) log file(s)
 * @param logParser             the parser for the format of logs found in inputPath
 * @param processingDefinitions specifies the function(s) for extracting information from / tranforming the logs as well as writers for the output data
 * @tparam D the type of the parsed log data
 * @tparam O the type of the output data, i.e. the type of the analyzed/extracted/summarized data from the log processing
 */
case class LogDataProcessor[D, O](inputPath: String, logParser: LogParser[D], processingDefinitions: Seq[DataProcessingDefinition[D, O]]) {

  /**
   * Runs the parser and processing functions against the log data and asynchronously writes to the associated output writers.
   *
   * @return Future indicating success or failure
   */
  def process()(implicit spark: SparkSession): Future[Seq[Unit]] = {
    val rawData = spark.read.textFile(inputPath)
    val parsedData = logParser.parseLog(rawData)
    parsedData.cache()

    val result = Future.sequence(processingDefinitions.flatMap { definition =>
      val output = definition.fn(parsedData)
      definition.outputWriters.map { writer =>
        writer.writeOutput(output)
      }
    })

    parsedData.unpersist()
    result
  }

}
