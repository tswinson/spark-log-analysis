package com.tswinson.utils

import com.typesafe.config.Config

import scala.reflect.ClassTag
import scala.util.{Failure, Success, Try}

object ConfigUtils {

  implicit class ConfigExtensions(val config: Config) extends AnyVal {

    /**
     * Retrieves a value conforming to the specified type from a typesafe Config object
     *
     * @param key     the key associated with the config value
     * @param isValid a function that determines whether the value is permissible
     * @tparam T the type of the value being queried
     * @return    the value associated with the specified key, if:
     *            - the key exists in the config, and
     *            - the associated value is of the specified type, and
     *            - the value is valid according to the isValid function
     *            otherwise, an exception is thrown
     */
    def getAs[T: ClassTag](key: String, isValid: T => Boolean): T = {
      Try(config.getAnyRef(key)) match {
        case Failure(_) => throw new Exception(s"could not find config value for [$key] in config")
        case Success(value: T) if !isValid(value) => throw new Exception(s"config value for [$key] is invalid")
        case Success(value: T) => value
        case _ => throw new Exception(s"config value for [$key] does not have specified type")
      }
    }
  }

}
