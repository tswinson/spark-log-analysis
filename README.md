# spark-log-analysis

[![pipeline status](https://gitlab.com/tswinson/spark-log-analysis/badges/master/pipeline.svg)](https://gitlab.com/tswinson/spark-log-analysis/-/commits/master)

[![coverage report](https://gitlab.com/tswinson/spark-log-analysis/badges/master/coverage.svg)](https://gitlab.com/tswinson/spark-log-analysis/-/commits/master)

This is an application that uses Apache Spark to determine the top-n most frequent items per group, occurring in log file(s), and the results are logged.

The group and item as well as output locations for the group is specified as a `DataProcessingDefinition`. The app is currently configured to produce the following result groups based on the data contained in a web server log file from the Internet Traffic Archive:

* top visitors by date
* top urls by date
* top response codes by date
* top urls by response code

Additional `DataProcessingDefinition`s can be added in `dataProcessingDefintions` of the main app, `WebLogAnalysis`. Additional output writers can be implemented by extending the `OutputWriter` trait.

#### Compiling
> sbt clean compile

>#### Testing
> sbt test

>#### Check style
> sbt scalastyle test:scalastyle

### Coverage reports
> sbt coverage test coverageReport coverageAggregate

#### Run locally in Docker
If you want to run the app locally, a docker image containing all dependencies including Spark can be created. First, create an image:
> sbt docker:publishLocal

Then, run with appropriate parameters, making sure to turn on local mode:
> docker run -m 8g registry.gitlab.com/tswinson/spark-log-analysis:0.1 --local-mode --input-path ftp://anonymous:anonymous@ftp-server/server-log.gz

If you encounter problems related to the app downloading from FTP within Docker, you can run from a local copy of the input file:
> wget -P data/ ftp://anonymous:anonymous@ftp-server/server-log.gz

> docker run -v \`pwd\`/data:/data -m 8g registry.gitlab.com/tswinson/spark-log-analysis:0.1 -l -i /data/server-log.gz

#### To assemble a fat JAR
To run on your Spark cluster of choice, you can create a jar file that includes the app plus all dependencies except Spark. Spark must be provided at runtime (e.g. installed on your cluster).
First, swap the commented line in `build.sbt` for the Spark dependency marked `% "provided"`. Then, run:
> sbt app/assembly

This will produce a jar file which can be submitted to your Spark cluster:
> target/scala-2.11/spark-log-analysis-assembly-0.1.jar

#### Logs
* uses Spark's logger

#### CI/CD
* Gitlab Pipeline steps defined in `.gitlab-ci.yaml`

### Parameters

* default configuration values for these parameters can be found in `reference.conf`

| param            | type        | desc |
| :------------- | :----------: | :---:|
| --num-top-urls | Int | number of most frequent urls per group to compute |
| --num-top-visitors | Int | number of most frequent visitors per group to compute |
| --num-top-response-codes | Int | number of most frequent response-codes per group to compute |
| --input-path | String | location of server logs (must be text format, gz compression is supported) |
| --local-mode | N/A | flag used when running the app locally (e.g., in docker on local machine without spark cluster) |
